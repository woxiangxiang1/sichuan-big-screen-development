import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/testpage",
  },
  {
    path: "/testpage",
    meta: {
      title: "开始页"
    },
    component: () => import("@/views/HelloWorld.vue")
  }
];


const router = new VueRouter({
  routes,
  base: import.meta.env.BASE_URL
});

export default router;

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || "四川大屏";
  next();
});
