import mapData from './map';
export default {
  state: {
    mapData
  },
  getters: {
    // 根据地图ID获取数据
    getMapData: state => chartId => {
      // for (let key in state.mapData) {
      //   let item = state.mapData[key];
      //   if (item.id == chartId) {
      //     return item
      //   }
      // }

      return state.mapData[chartId];
    },
    // 省局时，根据名称匹配
    getChartID2Name: state => name => {
      for (let key in state.mapData) {
        let item = state.mapData[key];
        if (item.name == name) {
          return item.swjg_dm
        }
      }
    },
    // 地市、区县根据税务机关匹配
    getChartID2SWJG: state => swjg_dm => {
      for (let key in state.mapData) {
        let item = state.mapData[key];
        if (item.swjg_dm == swjg_dm) {
          return item.swjg_dm
        }
      }
    },
    getMapItemByName: state => name => {
      for (let key in state.mapData) {
        let item = state.mapData[key];
        if (item.name == name || item.alias == name) {
          return item;
        }
      }
    },
    getMapItemBySWJG: state => swjg_dm => {
      for (let key in state.mapData) {
        let item = state.mapData[key];
        if (item.swjg_dm == swjg_dm) {
          return item;
        }
      }
    },
  },
  mutations: {
    setMapData(state, payload) {
      state.mapData = payload;
    }
  },
  actions: {
    setMapData({ state, commit, rootState }, payload) {
      commit("setMapData", payload);
    }
  }
};
