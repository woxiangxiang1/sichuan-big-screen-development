import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import ajax from "@/utils/request";
import "@/assets/styles/global.scss";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import dataV from "@jiaminghi/data-view";
import "@/assets/styles/elementUI-modify.scss";
import * as echarts from "echarts";

import {
  rowData2TreeData,
  getQueryVariable,
  listToTree
} from "@/utils/common";
import CONFIG from "@/utils/config";

// 注册自定义组件
import myComponents from '@/components/index.js';
myComponents.forEach(component => {
  Vue.use(component);
});

Vue.config.productionTip = false;
Vue.use(dataV);
Vue.use(ElementUI);

// 将请求添加到原型链上
Vue.prototype.ajax = ajax;
Vue.prototype.$axios = axios; // 将axios挂载到原型上方便使用
Vue.prototype.echarts = echarts;
/**
 * 使用时如下进行调用
 *  this.ajax({
      sql: ""
    }).then( res => {
      console.log(res)
    })
 */

const digtalFormat = (num) => {
  if (num) {
    num = (num || 0).toString();

    let val = Number(num);
    let neg = val < 0;
    if (neg) {
      num = num.substring(1);
    }

    let result = "";
    if (num.split(".")[1]) {
      result = "." + num.split(".")[1];
    }
    num = num.split(".")[0];
    while (num.length > 3) {
      result = "," + num.slice(-3) + result;
      num = num.slice(0, num.length - 3);
    }
    if (num) {
      result = num + result;
    }

    if (neg) {
      result = "-" + result;
    }

    return result;
  } else return num;
}

// 格式化数字为千分符展示
Vue.prototype.digtalFormat = digtalFormat;
// 转化数字单位,unit 单位，默认：元 , format 是否千分符展示, objType 是否返回Obj类型 
Vue.prototype.digtalFormatUnit = (number, unit, format, objType) => {
  if (typeof number != 'number') {
    return number
  }
  let num = Math.abs(number);
  let dig = (num || 0).toFixed(1);
  unit = unit || "元";
  //如果大于千亿，则用万亿表示
  if (num >= 1000000000000) {
    dig = (num / 1000000000000).toFixed(2);
    unit = "万亿" + unit;
  } else if (num >= 100000000) {
    dig = (num / 100000000).toFixed(2);
    unit = "亿" + unit;
  } else if (num >= 10000) {
    dig = (num / 10000).toFixed(2);
    unit = "万" + unit;
  }
  if (format) {
    dig = digtalFormat(dig);
  }

  if (objType) {
    let res = {};
    res.value = (number >= 0 ? '' : '-') + dig;
    res.unit = unit;
    return res;
  }
  return (number >= 0 ? '' : '-') + dig + ' ' + unit;
};

/**
 * 行结构数据转化为树形结构数据
 * @param {*} rows 
 * @param {*} config {
              id: 'id',// id字段
              pid: 'pid',// 上级id字段
              children: 'subNode',// 生成的下级节点key字段
              rootKey: '0' // 根节点上级id
            }
 * @returns 
 */
Vue.prototype.rowData2TreeData = rowData2TreeData;
Vue.prototype.listToTree = listToTree;

// 根据当前dom根字体换算对应字体大小
Vue.prototype.getSize = size => {
  return parseInt(size * store.state.remUnit / 16)
}

Date.prototype.format = function (fmt) {
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  }
  for (var k in o) {
    if (new RegExp("(" + k + ")").test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    }
  }
  return fmt;
}


Vue.prototype.$swjg_dm = getQueryVariable("swjg_dm") || CONFIG.defaultSwjg;

const EMPTY_OBJ = {};
new Vue({
  router,
  store,
  data() {
    return {
      pageStore: EMPTY_OBJ, // 页面临时存储，用于页面间跳转传参。
    }
  },
  methods: {
    getPageStore() {
      return this.pageStore;
    },
    setPageStore(store) {
      this.pageStore = store;
    },
    clearPageStore() {
      this.pageStore = EMPTY_OBJ;
    }
  },
  render: (h) => h(App),
}).$mount("#app");