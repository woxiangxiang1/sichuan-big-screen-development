export default {
    // 2022年3月28日17:54:39 从此废弃这里的url配置，放到运行环境去配
    // url: "http://155.16.171.225:80/nsrqsj/search.do", // 开发环境后台地址
    // url: "http://155.16.171.225:80/scsw/just_search.do", // 开发环境后台地址
    // prodUrl: "http://155.16.171.225:80/scsw/just_search.do", // 开发环境后台地址
    // prodUrl: "http://100.12.66.40:7010/zj/just_search.do", // 后台地址
    // prodUrl: "http://100.12.66.31:7010/zj/just_search.do", // 后台地址
    // url: "http://10.6.10.185:8081/nsrqsj/", // 开发环境后台地址
    retry: 1, // 请求查询失败后，重试查询次数
    retryDelay: 500, // 请求查询失败后，重试查询时间间隔 单位：ms
    isDemo: false, // true会阻止 this.ajax 发起请求,针对ipad打包时离线使用
    logOut: false, // 是否在控制台打印数据段
    remUnit: 16, // 默认html根字体大小
    defaultSwjg: "15100000000" // 默认税务机关
};