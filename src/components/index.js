import echarts from './echarts.vue'
import titleCompt from './titleCompt.vue'

const compontents = [{
  // echarts
  install: function (Vue) {
    Vue.component('echartCompt', echarts)
  }
  
},{
  // titleCompt
  install: function (Vue) {
    Vue.component('titleCompt', titleCompt)
  }
  
}]
export default compontents