# vue-platform

四川大屏前端框架脚手架

改造使用vite+vue2构建，Node版本不低于 16.1.0

## 若项目中无 node_modules文件夹，运行如下命令
```
npm install
```

### 安装完依赖后，运行如下命令启动项目
```
npm run start
```

### 打包项目
```
npm run build
```


插件库：
elementUI: https://element.eleme.cn/#/zh-CN/component/container
dataV   http://datav.jiaminghi.com/guide/borderBox.html#dv-border-box-3