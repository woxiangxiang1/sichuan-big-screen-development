import { defineConfig } from 'vite'
import legacy from '@vitejs/plugin-legacy'
import { createVuePlugin } from 'vite-plugin-vue2'
import path from "path";


export default defineConfig(({ command, mode }) => {

  const config = {
    plugins: [
      createVuePlugin(),
      // 浏览器兼容问题配置
      legacy({
        targets: ['chrome 73'],
        additionalLegacyPolyfills: ['regenerator-runtime/runtime'],
        renderLegacyChunks: true,
        polyfills: [
          'es.symbol',
          'es.promise',
          'es.promise.finally',
          'es/map',
          'es/set',
          'es.array.filter',
          'es.array.for-each',
          'es.array.flat-map',
          'es.string.replace-all',
          'es.object.define-properties',
          'es.object.define-property',
          'es.object.get-own-property-descriptor',
          'es.object.get-own-property-descriptors',
          'es.object.keys',
          'es.object.to-string',
          'web.dom-collections.for-each',
          'esnext.global-this',
          'esnext.string.match-all'
        ]
      }),
    ],
    resolve: {
      // 路径别名
      alias: {
        "@": path.resolve(__dirname, "src"),
      }
    },
    base: "/",
    build: {
      minify: true,
      sourcemap: true,
      outDir: "dist",
      chunkSizeWarningLimit: 1024000,
      // 打包时需要另外处理的commonjs规范的包
      commonjsOptions: {
        include: [
          /node_modules/, // 必须包含
        ],
      },
    },
    css: {
      preprocessorOptions: {
        // 这里配置 文件的全局引入
        scss: {
          additionalData: `@import "@/assets/styles/variable.scss";`
        }
      }
    },
    server: {
      port: 8899,
      // proxy: { // 代理配置
      //   "/bapi": {
      //     target: "http://10.0.42.73:8586",
      //     changeOrigin: true,
      //     rewrite: (path) => path.replace(/^\/bapi/, "")
      //   },
      // },
    },
    optimizeDeps: {
      // dataV引入报错； 开发时 解决这些commonjs包转成esm包
      include: [
        "@jiaminghi/c-render",
        "@jiaminghi/c-render/lib/plugin/util",
        "@jiaminghi/charts/lib/util/index",
        "@jiaminghi/charts/lib/util",
        "@jiaminghi/charts/lib/extend/index",
        "@jiaminghi/charts",
        "@jiaminghi/color",
      ],
    }
  }
  // 根据运行命令 注入环境变量信息,使其与webpack同样拥有 process.env
  if (mode === 'development') {
    // 开发环境
    config.define = {
      "process.env": {
        NODE_ENV: "development",
        VUE_APP_DB: "vertica2",
        VUE_APP_SCHEMA: "ap_scswdp",
        VUE_APP_SCHEMA_DI: "ap_scswdp",
        VUE_APP_PRODURL: "http://155.16.171.225:80/scsw/just_search.do"
      }
    }
  } else {
    // 生产环境
    config.define = {
      "process.env": {
        NODE_ENV: "production",
        VUE_APP_DB: "vertica2",
        VUE_APP_SCHEMA: "ap_scswdp",
        VUE_APP_SCHEMA_DI: "ap_scswdp",
        VUE_APP_PRODURL: "http://155.16.171.225:80/scsw/just_search.do"
      }
    }
  }
  return config
})
